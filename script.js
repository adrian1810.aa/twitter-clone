document.addEventListener("DOMContentLoaded", function (evt) {
  const boton = document.querySelector("button.tweetBox__tweetButton");
  // console.log(boton);

  boton.addEventListener("click", tweetButtonClicked);
  console.log(tweets);
  RecuperarTweets(tweets);
});

function crearNuevoPost(datosTweet) {
  // nombre: "",
  //   handle: "",
  //   imagen_url: "",
  //   avatar_url: "",
  //   texto_tweet: "",
  //   blue_ribbon: "",
  const cuentaVerificada = `
  <span class="material-icons post__badge"> 
  verified
  </span>`;
  return `
      <div class="post__avatar">
        <img src="${datosTweet.avatar_url}" alt="" />
      </div>

      <div class="post__body">
        <div class="post__header">
          <div class="post__headerText">
            <h3>
            ${datosTweet.nombre}
              <span class="post__headerSpecial">
                 ${datosTweet.verificado === true ? cuentaVerificada : ""}
                 ${datosTweet.handle} 
              </span>
            </h3>
          </div>  
          <div class="post__headerDescription">
            <p> ${datosTweet.texto_tweet} </p>
          </div>
        </div>
        ${
          datosTweet.imagen_url.length > 0
            ? '<img src=" ' + datosTweet.imagen_url + ' " alt="" />'
            : ""
        }
        <div class="post__footer">
          <span class="material-icons"> repeat </span>
          <span class="material-icons"> ${
            datosTweet.liked ? "favorite" : "favorite_border"
          }  </span>
          <span class="material-icons"> publish </span>
        </div>
      </div>
    `;
}

function tweetButtonClicked(evt) {
  const userDefault = {
    nombre: "Adrian",
    handle: "@adrian",
    imagen_url:
      "https://pbs.twimg.com/media/FwcgJTVWwAIOqc2?format=jpg&name=small",
    avatar_url:
      "https://pbs.twimg.com/profile_images/1640884494239698945/VAEfaUVE_400x400.jpg",
    texto_tweet: "",
    verificado: true,
  };

  evt.preventDefault();
  console.log("Hola mundo");

  const { value: textTweetValue } = document.querySelector("form input");
  console.log(textTweetValue);
  if (textTweetValue.length > 0) {
    userDefault.texto_tweet = textTweetValue;

    const destino = document.querySelector("div.feed");
    console.log(destino);

    const nuevoElemento = document.createElement("div");
    nuevoElemento.className = "post";
    nuevoElemento.innerHTML = crearNuevoPost(userDefault);

    // destino.appendChild(nuevoElemento);
    //Recupero el primer elemento  div con la clase post
    const otroHijo = document.querySelector("div.post");
    //Entonces aca lo que hacemos es insertar el  nuevo elemento
    //antes  de elemento  de "otro hijo"
    destino.insertBefore(nuevoElemento, otroHijo);

    document.querySelector("form input").value = "";

    const mensaje = document.querySelector("#mensaje");
    mensaje != null ? mensaje.remove() : console.log("No hay mensaje de error");
  } else {
    // Eliminamos el mensaje para que  no  se cree dos  span repetido
    const mensaje = document.querySelector("#mensaje");
    mensaje != null ? mensaje.remove() : console.log("No hay mensaje de error");

    const destino = document.querySelector("div.feed");
    const nuevoElemento = document.createElement("div");
    const icono = document.createElement("span");
    icono.className = "material-icons";
    icono.innerHTML = "error";
    nuevoElemento.className = "mensaje";
    nuevoElemento.innerHTML = "Error: El tweet no puede estar vacio";
    nuevoElemento.id = "mensaje";
    nuevoElemento.appendChild(icono);
    const otroHijo = document.querySelector("div.post");

    destino.insertBefore(nuevoElemento, otroHijo);

    document.querySelector("form input").value = "";
  }
}

const tweets = [
  {
    nombre: "Posteador 1",
    handle: "@posteador1",
    imagen_url:
      "https://pbs.twimg.com/media/FwcgJTVWwAIOqc2?format=jpg&name=small",
    avatar_url:
      "https://pbs.twimg.com/profile_images/1640884494239698945/VAEfaUVE_400x400.jpg",
    texto_tweet: "Primer Tweet",
    verificado: true,
    liked: true,
  },
  {
    nombre: "Posteador2",
    handle: "@posteador2",
    imagen_url: "",
    avatar_url:
      "https://pbs.twimg.com/profile_images/1365068242918780928/m278bxAZ_400x400.jpg",
    texto_tweet: "Segundo Tweet",
    verificado: false,
    liked: true,
  },
  {
    nombre: "Posteador3",
    handle: "@posteador3",
    imagen_url: "",
    avatar_url:
      "https://i.pinimg.com/originals/a6/58/32/a65832155622ac173337874f02b218fb.png",
    texto_tweet: "tercer tweet",
    verificado: false,
    liked: false,
  },
];

function RecuperarTweets(tweets_) {
  tweets_.forEach(function (tweet) {
    // console.log("posts", tweet);
    // return crearNuevoPost(tweet);

    const destino = document.querySelector("div.feed");
    const nuevoElemento = document.createElement("div");
    nuevoElemento.className = "post";
    nuevoElemento.innerHTML = crearNuevoPost(tweet);
    console.log("Nuevo elemenmto", nuevoElemento);

    const otroHijo = document.querySelector("div.post");
    console.log(otroHijo);
    if (otroHijo === null) {
      destino.appendChild(nuevoElemento);
    } else {
      destino.insertBefore(nuevoElemento, otroHijo);
    }
  });
}
